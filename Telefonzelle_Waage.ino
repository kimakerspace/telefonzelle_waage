int button1 = 2;
int button2 = 3;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(57600);
  pinMode(button1, INPUT_PULLUP);
  pinMode(button2, INPUT_PULLUP);

}

void loop() {
  // put your main code here, to run repeatedly:
  int button1State = digitalRead(button1);
  int button2State = digitalRead(button2);

  if (button1State == 1)
    {
      Serial.println("0");
      }
  else if (button2State == 1)
    {
      Serial.println("74");
    }
  else 
    {
      Serial.println("30");
    }
  
  delay(100);

}
